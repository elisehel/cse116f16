package ActionListeners;

import java.awt.event.ActionEvent;
/**
 * @author Nikhil Mithani 
 */
import java.awt.event.ActionListener;

import boardpieces.Room;
import gui.MainWindow;
import players.Players;
import players.TurnCounter;
//Allows the user to end their turn  

public class EndTurnListener implements ActionListener{
	
	MainWindow gui;
	TurnCounter counter;
	public EndTurnListener(MainWindow guim,TurnCounter c){
		gui = guim;
		counter = c;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		Players player = counter.getPlayer();
		
		

		if (player.getBE() instanceof Room){
			gui._makeSuggestion.setEnabled(true);
		} else {
			gui.hasRolled = false;
			counter.nextTurn();
			gui.updateDisplay();
			gui.populateButtonPanel();
		}
		
		
		
		//gui._makeSuggestion.setEnabled(true);
	}

}
