package ActionListeners;

import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import gui.MainWindow;
import players.Players;
import players.TurnCounter;
//Allows the user to move down on the board if the bottom tile is a hallway or room 

public class MoveDownListener implements ActionListener {

	public Players player;
	public MainWindow window;
	
	public MoveDownListener(TurnCounter count, MainWindow mainWindow){
		player = count.getPlayer();
		window = mainWindow;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (player.checkLegalDown() == false){
			JFrame frame = new JFrame();
			JPanel panel = new JPanel();
			JLabel label = new JLabel("Illegal Move!");
			panel.add(label);
			frame.add(panel);
			frame.pack();
			frame.setContentPane(panel);
			frame.setVisible(true);
		}
		if (player.getDie() > 0 && player.checkLegalDown()){
			player.setY(player.getY()+1);
			player.setDie(player.getDie()-1);
			window.populateBoardPanel(MainWindow.board);
			window.populateButtonPanel();
			window.updateDisplay();
		}
		
	}

}
