package ActionListeners;

import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import gui.MainWindow;
import players.Players;
import players.TurnCounter;
//Allows the user to move right on the board if the right tile is a hallway or room 

public class MoveRightListener implements ActionListener {

	Players player;
	MainWindow window;
	
	public MoveRightListener(TurnCounter count, MainWindow mainWindow){
		player = count.getPlayer();
		window = mainWindow;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (player.checkLegalRight() == false){
			JFrame frame = new JFrame();
			JPanel panel = new JPanel();
			JLabel label = new JLabel("Illegal Move!");
			panel.add(label);
			frame.add(panel);
			frame.pack();
			frame.setContentPane(panel);
			frame.setVisible(true);
		}
		if (player.getDie() > 0 && player.checkLegalRight()){
			player.setX(player.getX()+1);
			player.setDie(player.getDie()-1);
			window.populateBoardPanel(MainWindow.board);
			window.populateButtonPanel();
			window.updateDisplay();
		}
		
	}

}
