package ActionListeners;

import java.awt.event.ActionEvent; 

/**
 * @author Bryan Burrowes
 * Moves the current player up one space, decreases the current die roll by 1, then repopulates the board and button panels
 */
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import gui.MainWindow;
import players.Players;
import players.TurnCounter;
//Allows the user to move up on the board if the upper tile is a hallway or room 
public class MoveUpListener implements ActionListener {
	
	Players player;
	MainWindow window;
	
	public MoveUpListener(TurnCounter count, MainWindow mainWindow){
		player = count.getPlayer();
		window = mainWindow;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (player.checkLegalUp() == false){
			JFrame frame = new JFrame();
			JPanel panel = new JPanel();
			JLabel label = new JLabel("Illegal Move!");
			panel.add(label);
			frame.add(panel);
			frame.pack();
			frame.setContentPane(panel);
			frame.setVisible(true);
		}
		if (player.getDie() > 0 && player.checkLegalUp()){
			player.setY(player.getY()-1);
			player.setDie(player.getDie()-1);
			window.populateBoardPanel(MainWindow.board);
			window.populateButtonPanel();
			window.updateDisplay();
		}
		
	}

}
