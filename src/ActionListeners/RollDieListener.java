package ActionListeners;

import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener;

import gui.MainWindow;
import players.Players;
import players.TurnCounter;
//Allows the user to roll the die
public class RollDieListener implements ActionListener {
	
	private TurnCounter count;
	private MainWindow window;
	
	public RollDieListener(TurnCounter counter, MainWindow mainWindow){
		count = counter;
		window = mainWindow;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		window.hasRolled = true;
		Players player = count.getPlayer();
		player.setDie(player.rollDie());
		window.populateButtonPanel();
		window.updateDisplay();
		//System.out.println(player.getDie());
	}

}
