package ActionListeners;

import java.awt.GridLayout;
import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import gui.MainWindow;
import players.Players;
import players.TurnCounter;


/**
 * Allows the user to view the players hand
 * When 'view hand' button is clicked, a new window containing the 3 cards a player has comes up
 * @author Elise Helou, Bryan Burrowes 
 *
 */
public class ViewHandListener implements ActionListener{
	
	private TurnCounter count;
	private MainWindow window;
	private JFrame _frame;
	private JPanel _panel;
	
	public ViewHandListener(MainWindow mainWindow, TurnCounter counter){
		count = counter;
		window = mainWindow;
		
}	
	@Override
	public void actionPerformed(ActionEvent e) {
		_frame = new JFrame();
		_panel = new JPanel();
		_panel.setLayout(new GridLayout(3, 0));
		Players player = count.getPlayer();
		ArrayList<String> hand=player.getHand();
		System.out.println(player.getHand());
		JLabel l1 = new JLabel("" + hand.get(0));
		JLabel l2 = new JLabel("" + player.getHand().get(1));
		JLabel l3 = new JLabel("" + player.getHand().get(2));
		_panel.add(l1);
		_panel.add(l2);
		_panel.add(l3);
		_frame.add(_panel);
		_frame.setContentPane(_panel);
		_frame.setVisible(true);
		_frame.pack();
		
				
		
	}


}


