package ActionListeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;

import gui.MainWindow;
import players.Players;
import players.Suggestion;
import players.TurnCounter;

public class _MakeAccusationListener implements ActionListener {
	MainWindow gui;
	Suggestion sg;
	ArrayList<Players> players;
	TurnCounter counter;
	JFrame frame;
	JPanel panel;
	
	
	public _MakeAccusationListener(MainWindow guim,Suggestion sgi, ArrayList<Players> p, TurnCounter c) {
		 	gui = guim;
			sg = sgi;
			players = p;
			counter = c;
			frame = new JFrame();
			panel = new JPanel();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		
		String[] suspectName={"Mrs. Peacock","Mrs. White","Miss Scarlet","Prof. Plum","Col. Mustard", "Mr. Green"};
		String[] weaponName={"Knife","Revolver","Candlestick","Rope","Lead Pipe","Wrench"};
		String[] roomName={"Billiards Room","Conservatory","Library","Lounge","Ballroom","Dinding Room","Study","Hall"};
		//TODO 2 rooms are missing, I'm too lazy to figure it out
		String suspect = (String) JOptionPane.showInputDialog(frame, "Who do you think the killer is?","Suspect",JOptionPane.QUESTION_MESSAGE,null,suspectName,suspectName[0]);
		String weapon = (String) JOptionPane.showInputDialog(frame, "Which weapon do you think was used?","Weapon", JOptionPane.QUESTION_MESSAGE,null, weaponName,weaponName[0]);
		String room = (String) JOptionPane.showInputDialog(frame, "Which room do you think the murder took place?","Room", JOptionPane.QUESTION_MESSAGE,null, roomName,roomName[0]);
		//User story 3 #1 when suggestion true allow to make accusation
		if(sg.proveSuggestionFalse(players,suspect,weapon,room)){
			gui._makeAccusation.setEnabled(true);
		}
		//User story 3 #2 pop up you win and give option to restart the game or exit the game
		if(sg.makeAccusation()){
			JOptionPane.showMessageDialog(null, "Congratulations, You won!!!");
			int result = JOptionPane.showOptionDialog(null, "Would you like to play again", "Confirmation", JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE, null, null, null);
			if(result == JOptionPane.NO_OPTION){
				System.exit(0);
			}
			else if(result == JOptionPane.YES_OPTION){
				// restart the game
			}
			
			
		}
		//User story 3 #3 wrong accusation kicks player out the game
		else{
			JOptionPane.showMessageDialog(null, "Sorry, your accusation is incorrect, you lose");
			players.remove(counter.getPlayer());
		}
		
		frame.add(panel);
		frame.pack();
		frame.setContentPane(panel);
		frame.setVisible(true);
		
		gui.hasRolled = false;
		counter.nextTurn();
		gui.updateDisplay();
	}

}
