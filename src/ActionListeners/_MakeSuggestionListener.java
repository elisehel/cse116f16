package ActionListeners;

import java.awt.event.ActionEvent;

/**
 * @author Nikhil Mithani 
 */

import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import gui.MainWindow;
import players.Players;
import players.Suggestion;
//Allows user to make a suggestion at the end of their turn but only if they are in a room 
//When "make suggestion" button is clicked, user is able to type in the weapon, player, and room
import players.TurnCounter;

public class _MakeSuggestionListener implements ActionListener {
	//
	MainWindow gui;
	Suggestion sg;
	ArrayList<Players> players;
	TurnCounter counter;
	 public _MakeSuggestionListener(MainWindow guim,Suggestion sgi, ArrayList<Players> p, TurnCounter c) {
		// TODO Auto-generated constructor stub
		gui = guim;
		sg = sgi;
		players = p;
		counter = c;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		String[] suspectName={"Mrs. Peacock","Mrs. White","Miss Scarlet","Prof.plum","Col.Mustard"};
		String[] weaponName={"Knife","Revolver","Candlestick","Rope","Lead Pipe","Wrench"};
		String[] roomName={"Billiards Room","Conservatory","Library","Lounge","Ballroom","Dinding Room","Study","Hall"};
		String suspect = (String) JOptionPane.showInputDialog(null, "who do you think the killer is?","Suspect",JOptionPane.QUESTION_MESSAGE,null,suspectName,suspectName[0]);
		String weapon = (String) JOptionPane.showInputDialog(null, "Which weapon do you think was used?","Weapon", JOptionPane.QUESTION_MESSAGE,null, weaponName,weaponName[0]);
		String room = (String) JOptionPane.showInputDialog(null, "Which room do you think the murder took place?","Room", JOptionPane.QUESTION_MESSAGE,null, roomName,roomName[0]);
		if(sg.proveSuggestionFalse(players,suspect,weapon,room)){
			
		}
		
		gui.hasRolled = false;
		counter.nextTurn();
		gui.updateDisplay();
	}//

}
