package boardpieces;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author Bryan Burrowes (I wrote the whole boardpieces package) 
 */

public class ArrayBoard {
	
	private BoardElement[][] _board = new BoardElement[28][26];


	//Creates the entire board upon instantiating an ArrayBoard object
	public ArrayBoard(){	
		populateBoard();
	}
	
	/**
	 * 
	 * @param fileName
	 * @return Array of chars matching layout of board
	 */
	public char[][] populateArray(String fileName){
		String line = "";
		char[][] retVal = new char[28][26];
		try {
	            FileReader fileReader = new FileReader(new File(fileName));
	            BufferedReader bufferedReader = new BufferedReader(fileReader);
	            int lineNum = 0;
	
	            //Assigns each char to it's corresponding place in the array
	            while((line = bufferedReader.readLine()) != null && lineNum < 28) {
	                for (int i = 0; i < 26; i++){
	                	retVal[lineNum][i] = line.charAt(i);
	                }	
	                lineNum++;
	            }   
	
	            
	            bufferedReader.close();         
			}
	        catch(FileNotFoundException ex) {
	            System.out.println(
	                "Unable to open file '" + 
	                fileName + "'");                
	        }
	        catch(IOException ex) {
	            System.out.println(
	                "Error reading file '" 
	                + fileName + "'");                  
	        }
		return retVal;
	}
	/**
	 * Fills board using array from populateArray
	 */
	public void populateBoard(){
		String path = System.getProperty("user.dir") + "/src/boardpieces/board.txt";
		
		char[][] arr = populateArray(path);
		char w = 'W';
		char r = 'R';
		char d = 'D';
		char h = 'H';
		char p = 'P';
		char s = 'S';
		
		for (int i = 0; i < 28; i++){
			for (int j = 0; j < 26; j++){
				if (arr[i][j] == w){
					_board[i][j] = new Wall();
				}
				if (arr[i][j] == h){
					_board[i][j] = new Hallway();
				}
				if (arr[i][j] == d){
					_board[i][j] = new Door();
				}
				if (arr[i][j] == r){
					_board[i][j] = new Room();
				}
				if (arr[i][j] == p){
					_board[i][j] = new SecretPassage1();
				}
				if (arr[i][j] == s){
					_board[i][j] = new SecretPassage2();
				}
			}
		}
		
	}
	
	public BoardElement[][] getBoard(){
		return _board;
	}

	

	
}
