package boardpieces;

import players.Players;

public abstract class BoardElement {
	
	public abstract boolean hasPlayer();
	public abstract boolean setPlayer(Players newPlayer);
	

}
