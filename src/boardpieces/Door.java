package boardpieces;

import players.Players;

public class Door extends BoardElement{

	Players p;
	
	@Override
	public boolean hasPlayer() {
		return p != null;
	}

	@Override
	public boolean setPlayer(Players newPlayer) {
		if (newPlayer == null){
			p = null;
			return false;
		}
		p = newPlayer;
		return true;
	}

}
