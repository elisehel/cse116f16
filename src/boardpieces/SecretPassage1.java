package boardpieces;

import players.Players;

public class SecretPassage1 extends BoardElement{

	Players p;

	@Override
	public boolean hasPlayer() {
		return p != null;
	}

	@Override
	public boolean setPlayer(Players newPlayer) {
		if (newPlayer == null){
			p = null;
			return false;
		}
		p = newPlayer;
		return true;
	}

}
