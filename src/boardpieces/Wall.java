package boardpieces;

import players.Players;

public class Wall extends BoardElement{

	@Override
	public boolean hasPlayer() {
		return false;
	}

	@Override
	public boolean setPlayer(Players newPlayer) {
		return false;
	}

}
