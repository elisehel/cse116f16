package code;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
//Creates board 
//sets each room, the hallway, doors, secret passage ways, and border to a color
public class Board {
	/**
	 * @author Elise Helou
	 */
	

	 private static final int ROW = 28;
	 private static final int COL = 26;

		public static void main(String[] args) {

			Color[][] board;
            board = new Color[28][26];

		    JFrame frame = new JFrame("Clue");
		    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		    frame.setLayout(new GridLayout(ROW, COL, 2, 2));
            
		   
		    for (int i = 0; i<27; i++){
		    	for (int j=0; j<25; j++){
		    		//Border outside board
		    		if (i==0 && j>=0 && j<=25){
		    			board[i][j]= Color.BLUE;
		    		}
		    		if (i>=1 && i<=27 && j==0){
		    			board[i][j]= Color.BLUE;
		    		}
		    		if (i==27 && j>=0 && j<=25){
		    			board[i][j]= Color.BLUE;
		    		}
		    		if (i>=1 && i<=27 && j==25 ){
		    			board[i][j]= Color.BLUE;
		    		}
		    		//Border inside board
		    		if (i==1 && j>=9 && j<=10){
		    			board[i][j]= Color.BLUE;
		    		}
		    		if (i==1 && j>=15 && j<=16){
		    			board[i][j]= Color.BLUE;
		    		}
		    		if (i==1 && j==18){
		    			board[i][j]= Color.BLUE;
		    		}
		    		if (i==5 && j==1){
		    			board[i][j]= Color.BLUE;
		    		}
		    		if (i==7 && j==1){
		    			board[i][j]= Color.BLUE;
		    		}
		    		if (i==7 && j==24){
		    			board[i][j]= Color.BLUE;
		    		}
		    		if (i==9 && j==24){
		    			board[i][j]= Color.BLUE;
		    		}
		    		if (i>=12 && i<=13 && j==1){
		    			board[i][j]= Color.BLUE;
		    		}
		    		if (i==17 && j==24){
		    			board[i][j]= Color.BLUE;
		    		}
		    		if (i==19 && j==1){
		    			board[i][j]= Color.BLUE;
		    		}
		    		if (i==19 && j==24){
		    			board[i][j]= Color.BLUE;
		    		}
		    		if (i==23 && j==7){
		    			board[i][j]= Color.BLUE;
		    		}
		    		if (i==23 && j==18){
		    			board[i][j]= Color.BLUE;
		    		}
		    		if (i==24 && j>=1 && j<=9){
		    			board[i][j]= Color.BLUE;
		    		}
		    		if (i==24 && j>=16 && j<=25){
		    			board[i][j]= Color.BLUE;
		    		}
		    		//Study
		    		if (i>=1 && i<=4 && j>=1 && j<=7){
		    			board[i][j]= Color.RED;
		    		}
		    		//Library 
		    		if (i>=7 && i<=12 && j>=2 && j<=6){
		    			board[i][j]= Color.RED;
		    		}
		    		
		    		if (i>=8 && i<=11 && j==1){
		    			board[i][j]= Color.RED;
		    		}
		    		if (i>=8 && i<=11 && j==7){
		    			board[i][j]= Color.RED;
		    		}
		    		//Billiard Room
		    		if (i>=14 && i<=18 && j>=1 && j<=6){
		    			board[i][j]= Color.RED;
		    		}
		    		//Conservatory
		    		if (i==21 && j>=2 && j<=5){
		    			board[i][j]= Color.RED;
		    		}
		    		if (i>= 22 && i<=25 && j>=1 && j<=6 ){
		    			board[i][j]= Color.RED;
		    		}
		    		//Hall
		    		if (i==1 && j>=11 && j<=14){
		    			board[i][j]= Color.RED;
		    		}
		    		if (i>=2 && i<=7 && j>=10 && j<=15 ){
		    			board[i][j]= Color.RED;
		    		}
		    		//"CLUE"
		    		if (i>=9 && i<=15 && j>=10 && j<=14){
		    			board[i][j]= Color.RED;
		    		}
		    		//Ballroom
		    		if (i>=19 && i<=24 && j>=9 && j<=16){
		    			board[i][j]= Color.RED;
		    		}
		    		if (i>=25 && i<=26 && j>=11 && j<=14 ){
		    			board[i][j]= Color.RED;
		    		}
		    		//Lounge 
		    		if (i==1 && j>=19 && j<=24){
		    			board[i][j]= Color.RED;
		    		}
		    		if (i>=2 && i<=6 && j>=18 && j<=24 ){
		    			board[i][j]= Color.RED;
		    		}
		    		//Dining Room 
		    		if (i==16 && j>=20 && j<=24){
		    			board[i][j]= Color.RED;
		    		}
		    		if (i>=10 && i<=15 && j>=17 && j<=24 ){
		    			board[i][j]= Color.RED;
		    		}
		    		//Kitchen 
		    		if (i==19 && j>=19 && j<=23){
		    			board[i][j]= Color.RED;
		    		}
		    		if (i>=20 && i<=25 && j>=19 && j<=24 ){
		    			board[i][j]= Color.RED;
		    		}

		            JButton b=new JButton("" + board[i][j]);
		            
		            frame.add(b);
		        }
		    }
		        
		    frame.setSize(800, 800);
		    frame.setVisible(true);
		    
		}
    
}

