/**
 * @author Bryan Burrowes
 * @author Zhiyi Zhang
 */
package code;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;

	
public class Cards {
	
	static ArrayList<String> _suspects = new ArrayList<String>();
	static ArrayList<String> _weapons = new ArrayList<String>();
	static ArrayList<String> _rooms = new ArrayList<String>();
	static ArrayList<String> _deck = new ArrayList<String>();
	static ArrayList<String> _envelope = new ArrayList<String>();
	static ArrayList<String> _PlayerHand = new ArrayList<String>();
	
	public Cards(){
		_suspects.add("Mr. Green");
		_suspects.add("Mrs. Peacock");
		_suspects.add("Mrs. White");
		_suspects.add("Miss Scarlet");
		_suspects.add("Prof. Plum");
		_suspects.add("Col. Mustard");
		_weapons.add("Knife");
		_weapons.add("Revolver");
		_weapons.add("Candlestick");
		_weapons.add("Rope");
		_weapons.add("Lead Pipe");
		_weapons.add("Wrench");
		_rooms.add("Library");
		_rooms.add("Conservatory");
		_rooms.add("Billiards Room");
		_rooms.add("Kitchen");
		_rooms.add("Lounge");
		_rooms.add("Ballroom");
		_rooms.add("Dining Room");
		_rooms.add("Study");
		_rooms.add("Hall");
	}
	
	
	/**
	 * Separates the deck into three; suspects(i=0), weapons(i=1), and rooms(i=2)
	 * @return ArrayList of each deck
	 */
	public static ArrayList<ArrayList<String>> separate(){
		ArrayList<ArrayList<String>> sepDeck = new ArrayList<ArrayList<String>>();
		sepDeck.add(_suspects);
		Collections.shuffle(_suspects);
		sepDeck.add(_weapons);
		Collections.shuffle(_weapons);
		sepDeck.add(_rooms);
		Collections.shuffle(_rooms);
		return sepDeck;
	}
	
	/**
	 * Combines the three decks into one and shuffles
	 * @return 21 card full deck
	 */	 
	public ArrayList<String> combine() {
		_deck.addAll(_suspects);
		_deck.addAll(_weapons);
		_deck.addAll(_rooms);
		Collections.shuffle(_deck);
		return _deck;
	}
	
	/**
	 * Draws one card and removes it from the deck, use to deal at start of game
	 * @param deck drawn from
	 * @return the card drawn
	 */
	public String draw(ArrayList<String> deck){
		String card = deck.get(0);
		deck.remove(deck.get(0));
		return card;
	}
	
	/**
	 * create a arraylist of the 1st card in each deck at the beginning 
	 * of the game.
	 * 
	 * @author zhiyi zhang
	 */
	public void fillEnvelope(){
		Collections.shuffle(_weapons);
		Collections.shuffle(_suspects);
		Collections.shuffle(_rooms);
		_envelope.add(draw(_weapons));
		_envelope.add(draw(_suspects));
		_envelope.add(draw(_rooms));
	}
	
	/**
	 * return the instance of the envelope for solving the game
	 * 
	 * @return envelope
	 */
	public ArrayList<String> getEnvelope(){
		return _envelope;
	}
	
	public void fillPlayerHand(){
		Collections.shuffle(_weapons);
		Collections.shuffle(_suspects);
		Collections.shuffle(_rooms);
		_PlayerHand.add(draw(_weapons));
		_PlayerHand.add(draw(_suspects));
		_PlayerHand.add(draw(_rooms));
	}
	public ArrayList<String> getPlayerHand(){
		return _PlayerHand;
	}
	
}
