package code;

import java.util.Random;


/**
 * @author zhiyi zhang
 * @author nikhil mithnai
 */

public class Dice {
    
       private int die;   // Number showing on the die.
       private int side;  
       public Dice() {
               // Constructor.  Rolls the dice
           roll();  // Call the roll() method to roll the dice.
       }
       
       public int roll() {
               // Roll the dice by setting each of the dice to be
               // a random number between 1 and 6.
    	   side = 6;
           die = (int)(Math.random()*side) + 1;
           return die;
       }
                
       public int getDie() {
             // Return the number showing on the first die  .
          return die;
       }
       
       
       
    }  

