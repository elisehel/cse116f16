package code;

import java.util.ArrayList;

import boardpieces.ArrayBoard;
import gui.MainWindow;
import players.Players;
import players.TurnCounter;
//Runs gui
public class Driver {

	public static void main(String[] args) {
		ArrayBoard board = new ArrayBoard();
		
		Players p1 = new Players("Red");
		Players p2 = new Players("Purple");
		Players p3 = new Players("Green");
		Players p4 = new Players("White");
		Players p5 = new Players("Blue");
		Players p6 = new Players("Yellow");
		 
		Cards cards = new Cards();
		cards.fillEnvelope();
		ArrayList<String> deck = cards.combine();
		p1.addToHand(cards.draw(deck));
		p1.addToHand(cards.draw(deck));
		p1.addToHand(cards.draw(deck));
		System.out.println(p1.getHand());
		p2.addToHand(cards.draw(deck));
		p2.addToHand(cards.draw(deck));
		p2.addToHand(cards.draw(deck));
		p3.addToHand(cards.draw(deck));
		p3.addToHand(cards.draw(deck));
		p3.addToHand(cards.draw(deck));
		p4.addToHand(cards.draw(deck));
		p4.addToHand(cards.draw(deck));
		p4.addToHand(cards.draw(deck));
		p5.addToHand(cards.draw(deck));
		p5.addToHand(cards.draw(deck));
		p5.addToHand(cards.draw(deck));
		p6.addToHand(cards.draw(deck));
		p6.addToHand(cards.draw(deck));
		p6.addToHand(cards.draw(deck));
		
		ArrayList<Players> playerList = new ArrayList<Players>();
		playerList.add(p1);
		playerList.add(p2);
		playerList.add(p3);
		playerList.add(p4);
		playerList.add(p5);
		playerList.add(p6);
		
		
		
		MainWindow window = new MainWindow(board, playerList);
	}
	
}
