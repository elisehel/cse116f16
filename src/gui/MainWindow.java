package gui;

import java.awt.*;
import java.util.ArrayList;

/**
 * @author Bryan Burrowes
 */

import javax.swing.*;

import ActionListeners.*;
import ActionListeners._MakeSuggestionListener;
import boardpieces.ArrayBoard;
import boardpieces.BoardElement;
import boardpieces.Door;
import boardpieces.Hallway;
import boardpieces.Room;
import boardpieces.SecretPassage1;
import boardpieces.SecretPassage2;
import boardpieces.Wall;
import players.Players;
import players.Suggestion;
import players.TurnCounter;

/**
 * 
 * @author Bryan Burrowes
 *
 */
public class MainWindow {
	
	private JPanel _boardPanel;
	private JPanel _buttonPanel;
	private JPanel _mainPanel;
	private JPanel _displayPanel;
	private JPanel _rightPanel;
	private JFrame _frame;
	public JButton _makeSuggestion;
	public JButton _makeAccusation;
	private static ArrayList<Players> list;
	public static BoardElement[][] board;
	public static TurnCounter count;
	private Suggestion suggestion = new Suggestion();
	private JLabel dieroll;
	private JLabel turn;
	public boolean hasRolled = false; 
	
	

	public MainWindow(ArrayBoard boardClass, ArrayList<Players> playerList){
		board = boardClass.getBoard();
		list = playerList;
		count = new TurnCounter(playerList);
		
		dieroll = new JLabel("Roll: " + count.getPlayer().getDie());
		turn = new JLabel("Current Player: " + count.getName());
		
		_frame = new JFrame();
		_mainPanel = new JPanel();

		_mainPanel.setLayout(new FlowLayout());

		_boardPanel = new JPanel();
		_boardPanel.setLayout(new GridLayout(28, 26));
		_buttonPanel = new JPanel();
		_buttonPanel.setLayout(new GridLayout(0, 2));
		_displayPanel = new JPanel();
		_displayPanel.setLayout(new GridLayout(0, 1));
		_rightPanel = new JPanel();
		_rightPanel.setLayout(new GridLayout(2,0));
		
		_boardPanel.setPreferredSize(new Dimension(800, 600));
		
		_mainPanel.add(_boardPanel);
		_mainPanel.add(_rightPanel);
		_rightPanel.add(_buttonPanel);
		_rightPanel.add(_displayPanel);
		
		_displayPanel.add(turn);
		_displayPanel.add(dieroll);
		
		for(Players player : list){
			player.setInitialPositions();
		}
		
		populateButtonPanel();
		populateBoardPanel(board);
		
		updateDisplay();
		
		_frame.add(_mainPanel);
		_frame.pack();
		_frame.setContentPane(_mainPanel);
		_frame.setVisible(true);
		_frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	/**
	 * Adds all buttons to _buttonPanel
	 * @author Bryan Burrowes 
	 */
	public void populateButtonPanel(){
		
		_buttonPanel.removeAll();
		
		JButton rollDie = new JButton("Roll Die");
		rollDie.addActionListener(new RollDieListener(count, this));
		JButton viewHand = new JButton("View Hand"); 
		viewHand.addActionListener(new ViewHandListener(this, count));
		JButton endTurn = new JButton("End Turn");
		_makeSuggestion = new JButton("Make Suggestion");
		_makeSuggestion.setEnabled(false);
		endTurn.addActionListener(new EndTurnListener(this,count));
		_makeSuggestion.addActionListener(new _MakeSuggestionListener(this, suggestion,list,count));
		
		_makeAccusation = new JButton("Make Accusation");
		_makeAccusation.setEnabled(false);
		_makeAccusation.addActionListener(new _MakeAccusationListener(this, new Suggestion(), list, count));
		
		JButton moveUp = new JButton("Up");
		moveUp.addActionListener(new MoveUpListener(count, this));
		JButton moveDown = new JButton("Down");
		moveDown.addActionListener(new MoveDownListener(count, this));
		JButton moveLeft = new JButton("Left");
		moveLeft.addActionListener(new MoveLeftListener(count, this));
		JButton moveRight = new JButton("Right");
		moveRight.addActionListener(new MoveRightListener(count, this));
		
		if (count.getPlayer().getDie() <= 0){
			moveUp.setEnabled(false);
			moveDown.setEnabled(false);
			moveLeft.setEnabled(false);
			moveRight.setEnabled(false);
		}
		
		//Prevents player from rolling more than once
		if (hasRolled){
			rollDie.setEnabled(false);
		}
		
		
		_buttonPanel.add(rollDie);
		_buttonPanel.add(viewHand);
		_buttonPanel.add(endTurn);
		_buttonPanel.add(_makeSuggestion);
		_buttonPanel.add(_makeAccusation);
		_buttonPanel.add(moveUp);
		_buttonPanel.add(moveDown);
		_buttonPanel.add(moveLeft);
		_buttonPanel.add(moveRight);
		//TODO add ActionListeners to every button
	}
	
	/**
	 * Takes in an array of BoardElement, maps all values to a visual grid
	 * @author Bryan Burrowes
	 * @param board
	 */
	public void populateBoardPanel(BoardElement[][] board){
		
		_boardPanel.removeAll();
		_boardPanel.repaint();
		
		for (int i = 0; i < 28; i++){
			for (int j = 0; j < 26; j++){
				String s = "";
				JPanel p = new JPanel();
				
				//Colors every board space
				if (board[i][j] instanceof Wall){
					p.setBackground(Color.BLACK);
				}
				if (board[i][j] instanceof Room){
					p.setBackground(Color.RED);
				}
				if (board[i][j] instanceof Hallway){
					p.setBackground(Color.YELLOW);
				}
				if (board[i][j] instanceof Door){
					p.setBackground(Color.CYAN);
				}
				if (board[i][j] instanceof SecretPassage1){
					p.setBackground(Color.MAGENTA);
				}
				if (board[i][j] instanceof SecretPassage2){
					p.setBackground(Color.ORANGE);
				}
				
				//Places players on the board
				for (Players player : list){
					if (player.getX() == j && player.getY() == i){
						s = player.getColor();
						String c = "" + s.charAt(0);
						JLabel l = new JLabel(c);
						p.add(l);
						//TODO find better way to represent players
					}
				}
				
				_boardPanel.add(p);
			}
		} //end for loops//
	}
	
	/**
	 * Updates all information in the display Panel
	 * @author Bryan Burrowes
	 */
	public void updateDisplay(){	
		
		turn.setText("Current Player: " + count.getName());
		dieroll.setText("Roll: " + count.getPlayer().getDie());
		
		
	}
	

}
