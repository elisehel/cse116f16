/**
 * @author nikhilmithnai
 * @author Bryan Burrowes
 * @author zhiyi zhang
 */
package players;

import java.awt.Color;
import java.util.ArrayList;
import boardpieces.ArrayBoard;
import boardpieces.BoardElement;
import boardpieces.Door;
import boardpieces.Hallway;
import boardpieces.Room;
import boardpieces.SecretPassage1;
import boardpieces.SecretPassage2;
import code.Dice;

public class Players{
	//Characteristics of the Player pieces
	// choose how many players and which player u want to be 
	private String _name;
	private String _color;
	private ArrayList<String> _hand = new ArrayList<String>();
	private int x;
	private int y;
	private int dieroll = 0;
	private boolean _hasMoved;
	private BoardElement[][] board = (new ArrayBoard()).getBoard();
	
	public Players(String c){
		_hasMoved = false;
		_color = c;
		setName();	
	}
	
	/**
	 * @author nikhil mithnai 
	 */
	public void setName(){
		
		if (_color.equals("Yellow")){
			_name = "Col. Mustard";
		}	
	    if (_color.equals("Red")){
		    _name = "Miss Scarlet";
		}	
		if(_color.equals("Green")){
			_name = "Mr. Green";
		}
		if(_color.equals("Blue")){
			_name = "Mrs. Peacock";
		}
		if(_color.equals("White")){
			_name = "Mrs. White";
		}
		if(_color.equals("Purple")){
			_name = "Prof. Plum";
		}
		
	}
	
	
	public String getName() {
		return _name;	
	}
	
	public String getColor() {
		return _color;	
	}
	
	public Color getColorObj(){
		if (_color.equals("Yellow")){
			return Color.YELLOW;
		}	
	    if (_color.equals("Red")){
		    return Color.RED;
		}	
		if(_color.equals("Green")){
			return Color.GREEN;
		}
		if(_color.equals("Blue")){
			return Color.BLUE;
		}
		if(_color.equals("White")){
			return Color.BLACK;
		}
		if(_color.equals("Purple")){
			return Color.MAGENTA;
		} else {
			return null;
		}
	}
	
	public ArrayList<String> getHand() {
		return _hand;		
	}
	
	public boolean addToHand(String card) {
		return _hand.add(card);	
	}
	
	/* MAY BE UNNECESSARY LEFT JUST IN CASE
	 * 
	 * public boolean takeFromHand(String card) {
		if (!_hand.contains(card)){
			throw new NoSuchElementException();
		} else {
			return _hand.remove(card);
		}
		
	}*/
	
	/**
	 * Sets X value to given int
	 * @param i
	 */
	public void setX(int i){
		x=i;
	}
	
	/**
	 * Sets Y value to given int
	 * @param i
	 */
	public void setY(int i){
		y=i;
	}
	
	public int getX(){
		return x;
	}
	
	public int getY(){
		return y;
	}
	
	public BoardElement getBE(){
		return board[y][x];
	}
	
	
	/**
	 * Sets the starting positions of all players on the board.
	 * @author Bryan Burrowes 
	 */
	public void setInitialPositions(){
		if (_name == "Mrs. White"){
			setX(15);
			setY(26);
		}
		if (_name == "Miss Scarlet"){
			setX(17);
			setY(1);
		}
		if (_name == "Col. Mustard"){
			setX(24);
			setY(8);
		}
		if (_name == "Prof. Plum"){
			setX(1);
			setY(6);
		}
		if (_name == "Mrs. Peacock"){
			setX(1);
			setY(20);
		}
		if (_name == "Mr. Green"){
			setX(10);
			setY(26);
		}
		
		
	}
	
	/**
	 * Checks every time a player moves if they are on a passage
	 * @author Bryan Burrowes
	 */
	public void moveThroughPassage(){
		_hasMoved = true;
		if (getX() == 7 && getY() == 3){
			setX(20);
			setY(20);
		}
		if (getX() == 20 && getY() == 20){
			setX(7);
			setY(3);
		}
		if (getX() == 5 && getY() == 22){
			setX(18);
			setY(5);
		}
		if (getX() == 18 && getY() == 5){
			setX(5);
			setY(22);
		}	
	}

    /**
     * Methods to move the player
     * @author zhiyi zhang
     * @author Bryan Burrowes
     */
	
	public boolean moveUp(){
		if(dieroll == 0) return false;
		if(checkLegalUp()) {
			y--;			
			_hasMoved = true;
		}
		moveThroughPassage();
		dieroll--;
		return _hasMoved;
	}
	public boolean moveDown(){
		if(dieroll == 0) return false;
		if(checkLegalDown()) {
			y++;			
			_hasMoved = true;
		}
		moveThroughPassage();
		dieroll--;
		return _hasMoved;
	}
	public boolean moveLeft(){
		if(dieroll == 0) return false;
		if(checkLegalLeft()) {
			System.out.println(x);
			x--;
			System.out.println(x);
			_hasMoved = true;
		}
		moveThroughPassage();
		dieroll--;
		return _hasMoved;
	}
	public boolean moveRight(){
		if(dieroll == 0) return false;
		if(checkLegalRight()) {
			x++;
			_hasMoved = true;
		}
		moveThroughPassage();
		dieroll--;
		return _hasMoved;
	}
	
	/**
	 * Returns random value between 1 and 6
	 * @author Bryan Burrowes
	 * @return
	 */
	public int rollDie(){
		Dice die = new Dice();
		int retVal = die.roll();
		dieroll = retVal;
		return retVal;
	}
	
	/**
	 * For JUnit tests
	 */
	public void setDie(int i){
		dieroll = i;
	}
	
	public int getDie(){
		return dieroll;
	}

		
	/**
	 * 4 methods to check to all spaces around the Player to determine which directions are legal based 
	 * on the type of space the player is currently on
	 * 
	 * I realize these methods are not necessarily as efficient as they could be, but they work.
	 * 
	 * @author Bryan Burrowes
	 */
	public boolean checkLegalRight(){
		int newX = getX() + 1;
		if(newX >= 26) return false;
		BoardElement current = board[getY()][getX()];
		BoardElement next = board[getY()][newX];
		if(current instanceof Hallway) {
			if(next instanceof Hallway || next instanceof Door) { return true; }
			else { return false; }
		}
		else if(current instanceof Door) {
			if(next instanceof Room || next instanceof Hallway) { return true; }
			else { return false; }
		}
		else if(current instanceof Room) {
			if(next instanceof Door || next instanceof Room || next instanceof SecretPassage1 || next instanceof SecretPassage2) {
				return true;
			}
			else { return false; }
		}
		else if(current instanceof SecretPassage1 || current instanceof SecretPassage2) {
			if(next instanceof Room) { return true; }
			else { return false; }
		}
		return false;
	}
			
	public boolean checkLegalLeft(){
		int newX = getX() - 1;
		if(newX < 0) return false;
		BoardElement current = board[getY()][getX()];
		BoardElement next = board[getY()][newX];
		if(current instanceof Hallway) {
			if(next instanceof Hallway || next instanceof Door) { return true; }
			else { return false; }
		}
		else if(current instanceof Door) {
			if(next instanceof Room || next instanceof Hallway) { return true; }
			else { return false; }
		}
		else if(current instanceof Room) {
			if(next instanceof Door || next instanceof Room || next instanceof SecretPassage1 || next instanceof SecretPassage2) {
				return true;
			}
			else { return false; }
		}
		else if(current instanceof SecretPassage1 || current instanceof SecretPassage2) {
			if(next instanceof Room) { return true; }
			else { return false; }
		}
		return false;
	}
	
	public boolean checkLegalUp(){
		int newY = getY() - 1;
		if(newY < 0) return false;
		BoardElement current = board[getY()][getX()];
		BoardElement next = board[newY][getX()];
		if(current instanceof Hallway) {
			if(next instanceof Hallway || next instanceof Door) { return true; }
			else { return false; }
		}
		else if(current instanceof Door) {
			if(next instanceof Room || next instanceof Hallway) { return true; }
			else { return false; }
		}
		else if(current instanceof Room) {
			if(next instanceof Door || next instanceof Room || next instanceof SecretPassage1 || next instanceof SecretPassage2) {
				return true;
			}
			else { return false; }
		}
		else if(current instanceof SecretPassage1 || current instanceof SecretPassage2) {
			if(next instanceof Room) { return true; }
			else { return false; }
		}
		return false;
	}
	
	public boolean checkLegalDown(){
		int newY = getY() + 1;
		if(newY >= 28) return false;
		BoardElement current = board[getY()][getX()];
		BoardElement next = board[newY][getX()];
		System.out.println("Current position: " + current + " " + getY() + " " + getX());
		System.out.println("Next position: " + next + " " + newY + " " + getX());
		if(current instanceof Hallway) {
			if(next instanceof Hallway || next instanceof Door) { return true; }
			else { return false; }
		}
		else if(current instanceof Door) {
			if(next instanceof Room || next instanceof Hallway) { return true; }
			else { return false; }
		}
		else if(current instanceof Room) {
			if(next instanceof Door || next instanceof Room || next instanceof SecretPassage1 || next instanceof SecretPassage2) {
				return true;
			}
			else { return false; }
		}
		else if(current instanceof SecretPassage1 || current instanceof SecretPassage2) {
			if(next instanceof Room) { return true; }
			else { return false; }
		}
		return false;
	}
}