package players;

import java.lang.reflect.Array;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import code.Cards;

/**
 * 
 * @author zhiyi zhang
 * @author Bryan Burrowes
 */

public class Suggestion {
	
	//TODO distinguish between suggestion and accusation
	
	
	/**
	 * the name of the room the suggestion is based on
	 */
	private String roomName;
	
	/**
	 * the name of the suspect the suggestion is based on
	 */
	private String suspectName;
	
	/**
	 * the name of the weapon the suggestion is based on
	 */
	private String weaponName;
	
	Cards cards = new Cards();
	ArrayList<String> env = cards.getEnvelope();
	
	public Suggestion(){
		roomName = "";
		suspectName ="";
		weaponName = "";
	}//

//	/**
//	 * Must call in order to make suggestion
//	 * @return
//	 *Fix method to check the players hand instead of envelope
//	 */
//	public boolean makeSuggestion(){
//		if (env.get(0).equals(weaponName) && env.get(1).equals(suspectName) && env.get(2).equals(roomName)){
//			return true;
//		} else {System.out.println(weaponName + suspectName + roomName); return false;}		
//	}
	/**
	 * @author Nikhil Mithani
	 * @author Zhiyi Zhang
	 * Checks if any players have at least one card in their hand which is same as the suggestion cards. If 
	 * one of the player has the suggested card, then return that card
	 * else if you have gone through all the players hand and their hand has not matched any of the suggested cards then
	 * make accusation.
	 * This method is replaced the method above.
	 */
	public Boolean proveSuggestionFalse(ArrayList<Players>players,String suspect, String weapon,String room){
		String cardName="";
		for(int i = 0; i<players.size();i++){
			if(players.get(i).getHand().contains(room)){
				cardName = room;
			}
			else if (players.get(i).getHand().contains(suspect)){
				cardName = weapon;
			}
			else if (players.get(i).getHand().contains(weapon)){
				cardName = suspect;
			}
			else{
				
				//return makeAccusation();
				return true;
			}
		}
		//JOptionPane.showMessageDialog(null, "Your suggestion is False because "+suspect+" has the card "+cardName);
		//System.out.println(s);
				//System.out.println("Your suggestion is False because "+suspect+" has the card "+cardName);
		return false;
	}
		
	/* When no one has the cards you called in suggestion then call this method. Checks if suggestion is equal 
	 * to cards in envelope. If true you win. If false you are out of the game.
	 * 
	 */
	public boolean makeAccusation(){
		if (env.get(0).equals(weaponName) && env.get(1).equals(suspectName) && env.get(2).equals(roomName)){
			System.out.println("You Win!");
			return true;
		}
		 else {System.out.println(weaponName + suspectName + roomName); return false;}		
		
		
	}
	public String getRoom(){
		return roomName;
	}
	public String getWeapon(){
		return weaponName;
	}
	public String getSuspect(){
		return suspectName;
	}

}
