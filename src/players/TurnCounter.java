package players;

import java.util.ArrayList;

/**
 * 
 * @author Bryan Burrowes
 *
 */

public class TurnCounter {
	
	//Variables are static so that the current players remains the same 
	//until nextTurn is called
	private static int whosTurn;
	private static ArrayList<Players> list;
	
	
	public TurnCounter(ArrayList<Players> playerList){
		whosTurn = 0;
		list = playerList;
	}
	
	/**
	 * for JUnit, DO NOT USE THIS CONSTRUCTOR 
	 */
	public TurnCounter(){
		whosTurn = 0;
		Players p1 = new Players("Red");
		Players p2 = new Players("Purple");
		Players p3 = new Players("Green");
		Players p4 = new Players("White");
		Players p5 = new Players("Blue");
		Players p6 = new Players("Yellow");
		ArrayList<Players> pList = new ArrayList<Players>();
		pList.add(p1);
		pList.add(p2);
		pList.add(p3);
		pList.add(p4);
		pList.add(p5);
		pList.add(p6);
		list = pList;
	}
	
	public String getName(){
		return list.get(whosTurn).getName();
	}
	
	public Players getPlayer(){
		return list.get(whosTurn);
	}
	
	public void nextTurn(){
		if (whosTurn == list.size() - 1){
			whosTurn = 0;
		} else {
			whosTurn += 1;
		}	
	}

}
