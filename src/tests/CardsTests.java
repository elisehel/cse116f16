package tests;

import static org.junit.Assert.*;
import code.Cards;
import java.util.ArrayList;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;

/**
 * 
 * @author Bryan Burrowes
 *
 */
public class CardsTests {
	
	Cards _cards;
	
	@Before public void init(){
		_cards = new Cards();
	}

	@Test
	public void combineTest(){
		ArrayList<String> deck = _cards.combine();
		assertEquals(21, deck.size());
		assertTrue(deck.containsAll(_cards.combine()));
		
	}
	
	@Test
	public void separateTest(){
		ArrayList<ArrayList<String>> deck = _cards.separate();
		assertEquals(3, deck.size());
		ArrayList<String> suspects = deck.get(0);
		ArrayList<String> weapons = deck.get(1);
		ArrayList<String> rooms = deck.get(2);
		assertTrue(Collections.disjoint(rooms, weapons));
		assertTrue(Collections.disjoint(rooms, suspects));
		assertTrue(Collections.disjoint(weapons, suspects));

	}
	
	@Test
	public void drawTest(){
		ArrayList<String> deck = _cards.combine();
		String s = _cards.draw(deck);
		assertEquals(20, deck.size());
		assertNotNull(s);
	}
	
	
}
