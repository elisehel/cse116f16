package tests;

import static org.junit.Assert.*;
import org.junit.Test;
import players.Players;

/**
 * @author Elise Helou
 * @author Bryan Burrowes
 *
 */

public class FirstUserStoryTests {




@Test
	public void horizontalMovementTest(){
	    Players p1= new Players("Yellow");
	    p1.setX(8);
		p1.setY(6);
		p1.setDie(6);
		assertTrue(p1.checkLegalLeft());
		p1.moveLeft();
		assertEquals(7,p1.getX());
		assertTrue(p1.checkLegalRight());
		p1.moveRight();
		p1.moveRight();
		assertEquals(9,p1.getX());	
}


@Test
public void verticalMovementTest(){
    Players p1= new Players("Yellow");
	    p1.setX(8);
		p1.setY(6);
		p1.setDie(6);
		assertTrue(p1.checkLegalUp());
		p1.moveUp();
		assertEquals(5,p1.getY());
		assertTrue(p1.checkLegalDown());
		p1.moveDown();
		p1.moveDown();
		assertEquals(7,p1.getY());
}		
		
//
@Test
public void vertical_HorizontalMovementTest(){
    Players p1= new Players("Yellow");
	    p1.setX(8);
		p1.setY(6);
		p1.setDie(6);
		assertTrue(p1.checkLegalUp());
		p1.moveUp();
		assertEquals(5,p1.getY());
		assertTrue(p1.checkLegalDown());
		p1.moveLeft();
		assertEquals(7,p1.getX());
}


@Test
public void doorToRoomMovementTest(){
    Players p1= new Players("Yellow");
	    p1.setX(9);
		p1.setY(9);
		p1.setDie(6);
		assertTrue(p1.checkLegalLeft());
		p1.moveLeft();
		assertEquals(8,p1.getX());
		assertTrue(p1.checkLegalLeft());
		p1.moveLeft();
		assertEquals(7,p1.getX());
}


@Test
public void SecretPassagewayMovementTest(){
    Players p1= new Players("Yellow");
	    p1.setX(18);
		p1.setY(6);
		p1.setDie(6);
		assertTrue(p1.checkLegalUp());
		p1.moveUp();
		assertEquals(22,p1.getY());
	}

@Test
public void ExcessiveMovementTest(){
    	Players p1= new Players("Yellow");
	    p1.setX(18);
		p1.setY(2);
		p1.setDie(6);
		p1.moveDown();
		p1.moveDown();
		p1.moveDown();
		p1.moveDown();
		p1.moveDown();
		p1.moveDown();
		assertFalse(p1.moveDown());
}

@Test
public void DiagonalMovementTest(){
    //Diagonal movements are not possible as the only move methods specified are moveUp, moveDown, moveLeft, moveRight
}


@Test
public void NonContiguousMovementTest(){
    //Non-contiguous movements are not possible as the move methods increment X and Y values
}
@Test
public void WallMovementTest(){
    Players p1= new Players("Yellow");
    p1.setX(7);
	p1.setY(7);
	assertFalse(p1.moveLeft());
		
}

}