package tests;
/**
 * @author zhiyi zhang
 * @author Bryan Burrowes
 */
import players.Players;
import static org.junit.Assert.*;
import org.junit.Test;

import boardpieces.ArrayBoard;
import boardpieces.BoardElement;
import boardpieces.Door;
import boardpieces.Room;

public class PlayersTests {
	
	Players p = new Players("Yellow");
	
	@Test
	public void nameTest(){
		p.setName();
		assertEquals("Colonel Mustard", p.getName());
		Players pl = new Players("Red");
		assertEquals("Miss Scarlet", pl.getName());
	}
	
	@Test
	public void handTest(){
		p.addToHand("Knife");
		p.addToHand("Miss Scarlet");
		p.addToHand("Lounge");
		assertEquals("Knife", p.getHand().get(0));
		assertEquals(3, p.getHand().size());
	}

	@Test
	public void positionTest(){
		ArrayBoard board = new ArrayBoard();
		BoardElement[][] arr = board.getBoard();
		p.setX(10);
		p.setY(8);
		assertTrue(arr[p.getX()][p.getY()] instanceof Door);
		p.setX(4);
		p.setY(12);
		assertTrue(arr[p.getX()][p.getY()] instanceof Room);
	}
	
	@Test
	public void moveUp(){
		
	}
	
	@Test
	public void moveDown(){
		
	}
	
	@Test
	public void moveLeft(){
		
	}
	
	@Test
	public void moveRight(){
		
	}
	
	@Test
	public void entersRoom(){
		
	}
	
	@Test
	public void secretPassage(){
		
	}
	
	@Test
	public void moveDiagnal(){
		
	}
	
	@Test
	public void extraMoves(){
		
	}

	@Test
	public void noncontineousMove(){
		
	}
	
	@Test
	public void goesThruWall(){
		
	}
}
