package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import boardpieces.ArrayBoard;
import boardpieces.BoardElement;
import boardpieces.Hallway;
import boardpieces.Room;
import boardpieces.SecretPassage2;
import boardpieces.Wall;

/**
 * @author Elise Helou
 * @author Bryan Burrowes
 *
 */

public class RevisedBoardTests {

	ArrayBoard board = new ArrayBoard();
	BoardElement[][] boardArr = board.getBoard();
	char[][] arr = board.populateArray(System.getProperty("user.dir") + "/src/boardpieces/board.txt");

	@Test
	public void charValueCheck(){
		char w = 'W';
		char d = 'D';
		char r = 'R';
		assertEquals(w, arr[0][0]);
		assertEquals(d, arr[7][5]);
		assertEquals(w, arr[25][27]);
		assertEquals(r, arr[4][3]);
	}
	
	@Test
	public void boardCheck(){
		assertTrue(boardArr[0][0] instanceof Wall);
		assertTrue(boardArr[25][27] instanceof Wall);
		assertTrue(boardArr[4][9] instanceof Room);
		assertTrue(boardArr[10][8] instanceof Hallway);
		assertTrue(boardArr[5][22] instanceof SecretPassage2);
	}
	
}
