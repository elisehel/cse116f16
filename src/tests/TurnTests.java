package tests;
import static org.junit.Assert.*;
import org.junit.Test;

import players.TurnCounter;

/**
 * 
 * @author Bryan Burrowes
 *
 */

public class TurnTests { 
	
	@Test
	public void nextTurnTest(){
		TurnCounter count = new TurnCounter();
		assertEquals("Miss Scarlet", count.getName());
		count.nextTurn();
		assertEquals("Prof. Plum", count.getName());
		count.nextTurn();
		count.nextTurn();
		count.nextTurn();
		assertEquals("Mrs. Peacock", count.getName());
		count.nextTurn();
		count.nextTurn();
		assertEquals("Miss Scarlet", count.getName());
	}

}
